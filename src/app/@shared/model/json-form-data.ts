export interface JsonFormControls {
  name: string;
  label: string;
  value: string;
  type: string;
  options?: ControlOptions;
  required: boolean;
  validators: FormValidators;
}

interface ControlOptions {
  min?: string;
  max?: string;
  step?: string;
  icon?: string;
}

interface FormValidators {
  min?: number;
  max?: number;
  required?: boolean;
  email?: boolean;
  minLength?: boolean;
  maxLength?: boolean;
  pattern?: string;
  nullValidator?: boolean;
}
export interface JsonFormData {
  controls: JsonFormControls[];
}

