import { CommonModule } from '@angular/common';

import { JsonFormComponent } from './component/form/json-form/json-form.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CrudComponent } from './component/crud/crud.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorComponent } from './component/error/error/error.component';
import { CalendarModule } from 'primeng/calendar';
import {DialogModule} from 'primeng/dialog';
import {TableModule} from 'primeng/table';
import { ReplaceNullWithTextPipe } from './pipe/replace-null-with-text.pipe';
@NgModule({
  declarations: [CrudComponent, JsonFormComponent, ErrorComponent,ReplaceNullWithTextPipe],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,
    TableModule,
    DialogModule
  ],
  exports: [RouterModule, CrudComponent, JsonFormComponent,ReplaceNullWithTextPipe,DialogModule],

  providers: [],
})
export class SharedModule {}
