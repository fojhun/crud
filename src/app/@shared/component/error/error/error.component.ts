import { Component, Input, OnInit } from '@angular/core';
import { errorMassage } from 'src/app/@shared/model/error';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
@Input() errorMessage:string;
  constructor() { }

  ngOnInit(): void {
  }

}
