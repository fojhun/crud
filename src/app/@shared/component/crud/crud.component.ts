import { DataManager } from 'src/app/@shared/model/data-manager';

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})
export class CrudComponent implements OnInit {
  @Input() dataManager;
  HeaderTitle:string;
  displayBasic: boolean=false;
  constructor() { }

  ngOnInit(): void {
  }
  showBasicDialog() {
    this.displayBasic = true;
    this.HeaderTitle=`add ${this.dataManager.crudName}`;
}
}
