import { JsonFormService } from './../json-form.service';
import { JsonFormControls } from './../../../model/json-form-data';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { JsonFormData } from 'src/app/@shared/model/json-form-data';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-json-form',
  templateUrl: './json-form.component.html',
  styleUrls: ['./json-form.component.scss'],
})
export class JsonFormComponent implements OnInit, OnDestroy {
  FormData: JsonFormData;
  date1: Date;
  form: FormGroup = this.fb.group({});
  submitted: boolean = false;
  subscription: Subscription;
  constructor(
    private fb: FormBuilder,
    private jsonFormService: JsonFormService
  ) {}

  ngOnInit(): void {
    this.subscription = this.jsonFormService._controls$.subscribe((data) => {
      this.FormData = data;
      if (this.FormData) {
        this.createForm(this.FormData.controls);
      }
    });
  }

  createForm(controls: JsonFormControls[]) {
    for (const control of controls) {
      const validatorsToAdd = <any>[];
      for (const [key, value] of Object.entries(control.validators)) {
        switch (key) {
          case 'min':
            validatorsToAdd.push(Validators.min(value));
            break;
          case 'max':
            validatorsToAdd.push(Validators.max(value));
            break;
          case 'required':
            if (value) {
              validatorsToAdd.push(Validators.required);
            }
            break;
          case 'email':
            if (value) {
              validatorsToAdd.push(Validators.email);
            }
            break;
          case 'minLength':
            validatorsToAdd.push(Validators.minLength(value));
            break;
          case 'maxLength':
            validatorsToAdd.push(Validators.maxLength(value));
            break;
          case 'pattern':
            validatorsToAdd.push(Validators.pattern(value));
            break;
          case 'nullValidator':
            if (value) {
              validatorsToAdd.push(Validators.nullValidator);
            }
            break;
          default:
            break;
        }
      }

      this.form.addControl(
        control.name,
        this.fb.control(control.value, validatorsToAdd)
      );
    }
  }
  onSubmit() {
    this.submitted = true;
    if (this.form.valid) {
      this.jsonFormService.update_form(this.form.value);
    }
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
