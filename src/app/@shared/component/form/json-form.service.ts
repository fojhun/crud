import { JsonFormData } from 'src/app/@shared/model/json-form-data';
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class JsonFormService {
  private _form = new BehaviorSubject<any>(null);
  _form$ = this._form.asObservable();

  private _controls = new BehaviorSubject<JsonFormData>({ controls: [] });
  _controls$ = this._controls.asObservable();

  constructor() {}
  update_form(data: any) {
    this._form.next(data);
  }

  update_controls(controls: JsonFormData) {
    this._controls.next(controls);
  }
}
