export interface User {
  firstName: string;
  lastName: string;
  DateOfBirth: Date;
  PhoneNumber: string;
  email: string;
  BankAccountNumber: number;
}
