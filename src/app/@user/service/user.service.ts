import { JsonFormData } from 'src/app/@shared/model/json-form-data';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { RestClientService } from 'src/app/core/service/rest-client.service';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _users = new BehaviorSubject<User>({
    BankAccountNumber: 0,
    DateOfBirth: new Date(),
    email: '',
    firstName: '',
    lastName: '',
    PhoneNumber: '',
  });
   _users$ = this._users.asObservable();
  updateUser(user: User) {
    this._users.next(user);
  }

  constructor(private httpService: RestClientService) {}
  getControls() {
    let url = '/assets/data/user-form.json';

    return this.httpService.get<JsonFormData>(url).pipe(map((res) => res));
  }
}
