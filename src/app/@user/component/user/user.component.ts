import { User } from './../../model/user';
import { JsonFormService } from './../../../@shared/component/form/json-form.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { DataManager } from 'src/app/@shared/model/data-manager';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit, OnDestroy {
  dataManager = new DataManager();
  userList: User[] = [];
  subscription: Subscription;
  constructor(
    private UserService: UserService,
    private jsonFormService: JsonFormService
  ) {}

  ngOnInit(): void {
    this.getControls();
    this.getUserList();
  }
  getUserList() {
    this.subscription = this.jsonFormService._form$.subscribe((data) => {
      this.userList.push(data);
      this.dataManager.data = this.userList;
      this.dataManager.crudName = 'user';
    });
  }
  getControls() {
    this.UserService.getControls().subscribe((item) => {
      this.dataManager.column = item;
      this.jsonFormService.update_controls(item);
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
