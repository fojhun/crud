
import { Routes } from '@angular/router';
import { UserComponent } from './component/user/user.component';


export const userRoutes: Routes = [
  {
    path: '',
    component: UserComponent,

  },
];

export const routedComponents = [UserComponent];
